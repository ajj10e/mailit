# Mailit #

Mailit is a lightweight, self-hosted, file repository for all your email attachments built with PHP.

## How does it work? ##

Mailit fetches your email attachments from your imap mailbox and saves them locally, allowing you to access them offline through a simple bootstrap interface.

### Installation ###

**Requirements:**

* PHP 5
* Apache or Nginx

1. After installing PHP 5 and a web server.
2. Clone this repository into your web root.
3. That's it!

Note: depending on your configuration, it may be necessary to grant write permissions to your PHP user on the "files" directory.

### Notes ###
* This is alpha software and should not be used in a production environment.
* Mailit is not yet configured to use SSL. Do not use IMAP credentials that you need to be kept secure, even in a development environment. It is recommended that you create a "test" account with your email provider to work with Mailit.
* Mailit has only been tested with Gmail at the moment, although it is designed to work with most IMAP servers.

### Interested in contributing? ###
Read the Project Plan to get started:
https://docs.google.com/document/d/19vnPKlXIBbbBsIYS0LEtePR6I0VBmg1BSeRhDtTkIpw/edit?usp=sharing