<?php
require("php/header.php");
?>

<?php
require("php/nav.php");
?>

<div class="container">

	<div class="row content">
	<div class="col-xs-0 col-sm-1 col-md-2">
	</div>
	<div class="col-xs-12 col-sm-10 col-md-8">
		<div class="panel panel-default">
  			<!-- Default panel contents -->
  			<div class="panel-heading">Setup</div>
			<div class="panel-body">
				<form role="form" action="config.php" method="POST">
					<div class="form-group">
    					<label for="imap">IMAP Server:</label>
    					<input type="text" class="form-control" name="imap">
  					</div>
  					<div class="form-group">
    					<label for="port">Port:</label>
    					<input type="number" class="form-control" name="port" min="1" max="9999">
  					</div>
  					<div class="form-group">
    					<label for="username">Username:</label>
    					<input type="text" class="form-control" name="username">
  					</div>
  					<div class="form-group">
    					<label for="password">Password:</label>
    					<input type="password" class="form-control" name="password">
  					</div>
  					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
  			
		</div>
	</div><!-- /.col -->
	</div><!-- /.content -->

</div><!-- /.container -->

<?php
require("php/footer.php");
?>