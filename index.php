<?php
require("php/header.php");
if(!file_exists("config.txt")) {
	die(header('Location: setup.php'));
}
?>

<?php
require("php/nav.php");
?>

<div class="container">

	<div class="row content">
	<div class="col-xs-0 col-sm-1 col-md-2">
	</div>
	<div class="col-xs-12 col-sm-10 col-md-8">
		<div class="panel panel-default">
  			<!-- Default panel contents -->
  			<div class="panel-heading">Files</div>

  			<!-- Table -->
  			<table class="table">
  				
  				<tbody>
<?php

$dataDir  = dirname(__FILE__).'/files/';

try
{
	$dir  = new DirectoryIterator($dataDir);

    foreach ($dir as $file)
    {
    	if (!$file->isDot()) {
    		echo '<tr>';
    		$filename = $file->getFilename();
        	echo '<td><span class="glyphicon glyphicon-file" aria-hidden="true"></span> '.$filename.'</td>';
        	echo '<td class="open"><a class="btn btn-success" href="./files/'.$filename.'">Open</a></td>';
        	echo '</tr>';
        	
		}
    }
}
catch (Exception $ex)
{

}


?>
				</tbody>
  			</table>
		</div>
	</div><!-- /.col -->
	</div><!-- /.content -->

</div><!-- /.container -->

<?php
require("php/footer.php");
?>